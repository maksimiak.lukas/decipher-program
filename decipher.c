#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* defining special characters */
#define SPACE 32
#define CHAR_M 77
#define CHAR_Z 90

int main(int argc, int * argv[]) {

  //validate arguments
  if (argc < 2) {
    printf("Please provide input file\n");
    exit(EXIT_FAILURE);
  }
  if (argc > 2) {
    printf("Too many arguments\n");
    exit(EXIT_FAILURE);
  }

  char * CIPHER_ABC = "URWOUPASKDFGYTJHNCIXLEBQZM";
  char * ENG_ABC = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  char * pch;

  FILE * fp, * wfp; //file for reading
  fp = fopen((const char * ) argv[1], "r");
  wfp = fopen("atsakymas.txt", "w");

  if (fp == NULL) {
    exit(EXIT_FAILURE);
  }
  if (wfp == NULL) {
    exit(EXIT_FAILURE);
  }

  int c = getc(fp);
  pch = strchr(ENG_ABC, c);
  int index = pch - ENG_ABC;
  int print_flag = 1;
  while (c != EOF) {
    if (print_flag)
      fputc(CIPHER_ABC[index], wfp);

    c = getc(fp);

    //handle spaces
    if (c == SPACE) {
      fputc(CHAR_M, wfp);
      print_flag = 0;
      continue;
    }

    //search for specific symbol
    char * pch0 = strchr(ENG_ABC, c);

    //handle special symbols
    if (pch0 == NULL) {
      fputc(c, wfp);
      print_flag = 0;
    } else {
      if (c == CHAR_Z) {
        fputc(SPACE, wfp);
        print_flag = 0;
      } else {
        print_flag = 1;
      }
    }
    index = pch0 - ENG_ABC;

  }
  fclose(fp);
  fclose(wfp);

  return 0;
}