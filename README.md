# Simple decipher program

This simple decipher program has predifined cipher alphabet which is used to decipher cipher text. Default alphabet is Latin (A - Z).
Input file is ciphertext and output file is original (deciphered) text. Both files are represented in .txt format.


## Prerequisites

Make sure you have installed gcc: https://gcc.gnu.org/install/download.html
Also make sure you have your ciphertext .txt file ready in your compiling directory.

## Compiling and running

Linux
```bash
gcc decipher decipher.c 
./decipher File
```
Windows
```bash
gcc decipher decipher.c
decipher.exe File
```

## Usage

Specify the file you want to decipher. 
You will get an output .txt file with decrypted text.

## Comments

This decipher program uses only one and only decryption alphabet which is 100% deterministic. 
This program was built only for educational purposes and for a specific task.
